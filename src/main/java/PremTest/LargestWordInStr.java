package PremTest;

import java.util.Arrays;
import java.util.Comparator;

public class LargestWordInStr {

    public static void main(String[] args) {
        String[] str = {"karthikkumar", "hi", "hello", "ab", "ad", "qwertyuiopikjhgfc", "English", "Math", "unitates"};

        String s=  Arrays.stream(str).max(Comparator.comparing(String::length)).orElse("Empty ");

        System.out.println(s);
//        int um =542681;
//        var i = String.valueOf(um).chars().map(Character::getNumericValue).max();
//        System.out.println(i);
    }
}
