package PremTest;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class SecondHighSalary {
    public static void main(String[] args) {
        Map<String, Integer> salaryMap = new HashMap<>();

        // Populate the HashMap with employee salaries
        salaryMap.put("Alice", 50000);
        salaryMap.put("Bob", 60000);
        salaryMap.put("Guru", 75000);
        salaryMap.put("Charlie", 55000);
        salaryMap.put("David", 80000);
        salaryMap.put("Eve", 65000);
        salaryMap.put("Karthik", 65000);

        var first = salaryMap
                .entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .skip(1)
                .findFirst()
                .get();
        System.out.println(first);
    }
}
