package PremTest;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Flappy {


    public static void main(String[] args) {
        String name = "flappybirdflying";

        name.chars().mapToObj(e -> (char) e).collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                .entrySet().stream().filter(e -> e.getValue() > 1)
                .sorted(Map.Entry.comparingByKey())
                .forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));

//        List<Integer> num = List.of(10, 15, 5, 10, 98, 5, 98, 32);
//
//        num.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()))
//                .entrySet().stream().filter(e -> e.getValue() > 1)
//                .forEach(e -> System.out.println(e.getKey() + " " + e.getValue()));


    }

}
