package comparator_comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ComparableMain implements Comparable<ComparableMain> {
    private String name;
    private int id;

    public ComparableMain(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "Main{" +
                "name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    @Override
    public int compareTo(ComparableMain o) {
        if (id == o.id){
            return 0;
        } else if (id>o.id){
            return 1;
        }else
            return -1;
    }

    public static void main(String[] args) {
        List<ComparableMain> list = new ArrayList<>();

        list.add(new ComparableMain("Karthik",24));
        list.add(new ComparableMain("Arjun",6));
        list.add(new ComparableMain("Saravana",21));
        list.add(new ComparableMain("Prem",21));

//        Collections.sort(list);
        //sorting is based on id
//        for (ComparableMain com : list){
//            System.out.println(com.getId()+" "+com.getName());
//        }

        System.out.println("sorting based on name if Id is same");
        list.sort(new ComparatorMain());
        for (ComparableMain com : list){
            System.out.println(com.getName()+" "+com.getId());
        }

    }
}
