package comparator_comparable;

import java.util.ArrayList;
import java.util.List;

public class IgnnoreTableContentsIfNot {
    public static void main(String[] args) {
        List<String> inputLines = new ArrayList<>();
        inputLines.add("101");
        inputLines.add("# Cars");
        inputLines.add("## Mustang");
        inputLines.add("Cars came into global use during the 20th century");
        List<String> toc = generateTableOfContents(inputLines);
        for (String line : toc) {
            System.out.println(line);
        }
    }

    public static List<String> generateTableOfContents(List<String> text) {
        List<String> list = new ArrayList<>();
        int cCount = 0;
        int sCount = 0;

        for (String line : text) {
            if (line.startsWith("# ")) {
                cCount++;
                sCount = 0;
                list.add(cCount + ". " + line.substring(2));
            } else if (line.startsWith("## ")) {
                sCount++;
                list.add(cCount + "." + sCount + ". " + line.substring(3));
            }
        }
        return list;
    }
}

