package comparator_comparable;

import java.util.Comparator;

public class ComparatorMain implements Comparator<ComparableMain> {
    @Override
    public int compare(ComparableMain o1, ComparableMain o2) {
        if (o1.getId()==o2.getId()){
            return o1.getName().compareTo(o2.getName());
        }
        else if (o1.getId()> o2.getId()){
            return 1;
        }
        else {
            return -1;
        }
    }
}
