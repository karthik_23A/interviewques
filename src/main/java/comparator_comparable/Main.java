package comparator_comparable;

import java.util.*;

import java.util.ArrayList;
import java.util.HashMap;

public class Main {
    public static List<Integer> findOccurrences(int x, List<Integer> arr, List<Integer> queryValues) {
        Map<Integer, List<Integer>> occurrences = new HashMap<>();
        List<Integer> result = new ArrayList<>();
        for (int i = 0; i < arr.size(); i++) {
            if (arr.get(i) == x) {
                if (!occurrences.containsKey(x)) {
                    occurrences.put(x, new ArrayList<>());
                }
                occurrences.get(x).add(i + 1);
            }
        }
        for (int query : queryValues) {
            if (!occurrences.containsKey(x) || query > occurrences.get(x).size()) {
                result.add(-1);
            } else {
                result.add(occurrences.get(x).get(query - 1));
            }
        }
        return result;
    }


    public static void main(String[] args) {
        int x = 5;
        List<Integer> arr = Arrays.asList(5, 2, 5, 7, 5, 3, 5);
        List<Integer> queryValues = Arrays.asList(1, 2, 3, 4, 5);

        List<Integer> result = findOccurrences(x, arr, queryValues);

        System.out.println("Occurrences of " + x + " in the array: " + arr);
        System.out.println("Query values: " + queryValues);
        System.out.println("Results:");
        for (int i = 0; i < queryValues.size(); i++) {
            System.out.println("Query " + queryValues.get(i) + ": " + result.get(i));
        }
    }
}

