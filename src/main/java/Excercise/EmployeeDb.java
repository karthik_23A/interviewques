package Excercise;

import java.util.ArrayList;
import java.util.List;

public class EmployeeDb {
    public static List<Employee> getEmployees() {
        List<Employee> data = new ArrayList<>();
        data.add(new Employee(1,"Karthik", 26500));
        data.add(new Employee(2,"Ganga", 43500));
        data.add(new Employee(3,"Ravi", 22500));
        data.add(new Employee(4,"Siva", 22500));
        data.add(new Employee(5,"Prem the great", 1250000));


        return data;
    }
}
