package Excercise;

import java.util.Scanner;

public class ProductAndSum {
    private static void PrintSumOfProduct(int arr_size, int[] num){
        Scanner sc=new Scanner(System.in);
        int res =1,rem=0,fres=0;
        for (int i=0;i<arr_size;i++){
            num[i] = sc.nextInt();
        }

        // addition of result of multiplication
        for (int i=0;i<arr_size;i++){
            if (num[i]!=0){
                res *= num[i];

                rem = res %10; //rem = 60480
                fres += rem;

            }
        }
        System.out.println("Product is"+res);
        System.out.println("Sum is:"+fres);
    }
    public static void main(String[] args) {
        int arr_size =new Scanner(System.in).nextInt();
        int[] num=new int[arr_size];
        PrintSumOfProduct(arr_size,num);

    }
}
