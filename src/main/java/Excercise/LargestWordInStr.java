package Excercise;

import java.util.Arrays;
import java.util.Comparator;
import java.util.stream.Collectors;

public class LargestWordInStr {
    public static void main(String[] args) {
        String[] str = {"karthikkumar", "hi", "hello", "ab", "ad", "qwertyuiopikjhgfc", "English", "Math", "unitates"};
//        String str1 = "karthikkumar hi hello ab ad qwertyuiopikjhgfc English Matth unitates";
        String s = Arrays
                .stream(str)
                .max(Comparator.comparingInt(String::length))
                .orElse("Empty String");
      //System.out.println(s);

        int[] nu={10,12,15,1,8,8};
        Arrays.stream(nu)
                .boxed()
                .collect(Collectors.groupingBy(e->e,Collectors.counting()))
                .forEach((k,v)-> System.out.println(k+" "+v));

        int num=5832;
        var max = String.valueOf(num)
                .chars()
                .map(Character::getNumericValue)
                .max()
                .orElse(-1);
//        System.out.println(max);

    }
}
