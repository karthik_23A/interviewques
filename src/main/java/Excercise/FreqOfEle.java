package Excercise;

import java.util.Arrays;
import java.util.stream.Collectors;

public class FreqOfEle {


    //    static void freq(int arr[],int len){
//        Map<Integer,Integer> ele = new HashMap<>();
//        for (int i=0;i<len;i++){
//            if (ele.containsKey(arr[i])){
//                ele.put(arr[i], ele.get(arr[i])+1);
//            }
//            else {
//                ele.put(arr[i],1 );
//            }
//        }
//        for (Map.Entry<Integer,Integer> entry: ele.entrySet()){
//            System.out.println(entry.getKey()+" "+entry.getValue());
//        }
//    }
    public static void main(String[] args) {

        int arr[] = {1, 2, 1, 5, 7, 2, 2, 5};
//        int len = arr.length;
//        freq(arr,len);
                Arrays.
                stream(arr).
                boxed().
                collect(Collectors.groupingBy(e -> e, Collectors.counting())).
                entrySet().
                stream().
                filter(e -> e.getValue() >= 1).
                forEach(s -> System.out.println(s.getKey()));

    }
}
