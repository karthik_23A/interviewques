package Excercise;

public class Palin {
    static boolean isPalin(String ip){
        int left = 0;
        int right = ip.length()-1;
        while (left<right){
            if (ip.charAt(left)!=ip.charAt(right))
                return false;
            left++;
            right--;
        }
        return true;
    }
    public static void main(String[] args) {
        String st = "ra1r";
        if (isPalin(st))
            System.out.println("palin");
        else
            System.out.println("not palin");

    }
}
