package Excercise.NewOne;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

    public static void main(String[] args) {
        List<Student> studentList = Stream.of(
                new Student(1, "Rohit", 30, "Male", "Mechanical Engineering", "Mumbai", 122, Arrays.asList("+912632632782", "+1673434729929")),
                new Student(2, "Pulkit", 56, "Male", "Computer Engineering", "Delhi", 67, Arrays.asList("+912632632762", "+1673434723929")),
                new Student(3, "Ankit", 25, "Female", "Mechanical Engineering", "Kerala", 164, Arrays.asList("+912632633882", "+1673434709929")),
                new Student(4, "Satish Ray", 30, "Male", "Mechanical Engineering", "Kerala", 26, Arrays.asList("+9126325832782", "+1671434729929")),
                new Student(5, "Roshan", 23, "Male", "Biotech Engineering", "Mumbai", 12, Arrays.asList("+012632632782")),
                new Student(6, "Chetan", 24, "Male", "Mechanical Engineering", "Karnataka", 90, Arrays.asList("+9126254632782", "+16736784729929")),
                new Student(7, "Arun", 26, "Male", "Electronics Engineering", "Karnataka", 324, Arrays.asList("+912632632782", "+1671234729929")),
                new Student(8, "Nam", 31, "Male", "Computer Engineering", "Karnataka", 433, Arrays.asList("+9126326355782", "+1673434729929")),
                new Student(9, "Sonu", 27, "Female", "Computer Engineering", "Karnataka", 7, Arrays.asList("+9126398932782", "+16563434729929", "+5673434729929")),
                new Student(10, "Shubham", 26, "Male", "Instrumentation Engineering", "Mumbai", 98, Arrays.asList("+912632646482", "+16734323229929")))
                .collect(Collectors.toList());

        // 1. Find the list of students whose rank is in between 50 and 100
        List<Student> collect = studentList
                .stream()
                .filter(s -> s.getRank() >= 50 && s.getRank() <= 100)
//                .map(Student::getRank)
                .collect(Collectors.toList());
//        System.out.println(collect);

        //2.Find the Students who stays in Karnataka and sort them by their names
        var data = studentList
                .stream()
                .filter(s -> s.getCity().equals("Karnataka"))
                .sorted(Comparator.comparing(Student::getFirstName))
                .collect(Collectors.toList());
//        System.out.println(data);

        //3. Find all dept names
        var deptNames = studentList
                .stream()
                .map(Student::getDept)
                .distinct()
                .collect(Collectors.toList());
//        System.out.println(deptNames);

        //4.  Find all the contact numbers
        //one student has many contacts are available so, need to use flatMap instead of Map
        //oneToMany(One Student have -> List of contacts)
        //oneToOne(One Student have -> one id,Name,dept name)
        var contactNums = studentList
                .stream()
                .flatMap(s -> s.getContacts().stream())
                .collect(Collectors.toList());
//        System.out.println(contactNums);

        //Check if phno present or not
        String s2 = studentList
                .stream()
                .flatMap(s -> s.getContacts().stream().filter(s1 -> s1.equals("+912632632782")))
                .findAny().get();
        System.out.println(s2);

        //5. Group The Student By Department Names
        var dept = studentList.stream().collect(Collectors.groupingBy(Student::getDept));
//        System.out.println(dept);

        //6. Find the department who is having maximum number of students
        var max = studentList
                .stream()
                .collect(Collectors.groupingBy(Student::getDept, Collectors.counting()))
                .entrySet()
                .stream().max(Map.Entry.comparingByValue()).get();

//        System.out.println(max);

        //7. avg age male and female emp
        var avgAgeOfStudents=studentList
                .stream()
                .collect(Collectors.groupingBy(Student::getGender,Collectors.averagingDouble(Student::getAge)));
//        System.out.println(avgAgeOfStudents);

        //8. Find the highest rank in each department

        var res=studentList
                .stream()
                .collect(Collectors.groupingBy(Student::getDept,Collectors.minBy(Comparator.comparing(Student::getRank))));
//        System.out.println(res);

        //9.Find the student who has second rank
//        studentList
//                .stream()
//                .sorted(Comparator.comparing(Student::getRank))
//                .skip(1)
//                .limit(1)
//                .forEach(System.out::println);


    }
}
