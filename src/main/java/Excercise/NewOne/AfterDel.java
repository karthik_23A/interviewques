package Excercise.NewOne;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class AfterDel {
    public static void main(String[] args) {
        int[] num = {1,2,2,3,3,4,4,1,2,3};
        Map<Integer, Long> collect = Arrays
                .stream(num).boxed()
                .collect(Collectors.groupingBy(e->e, Collectors.counting()));
        System.out.println(collect);
    }


}
