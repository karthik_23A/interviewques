package Excercise.NewOne;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class HighestSalary {

    public static void main(String[] args) {

        List<Student> studentList = Stream.of(
                new Student(1, "Rohit", 30, "Male", "Mechanical Engineering", "Mumbai", 122, Arrays.asList("+912632632782", "+1673434729929")),
                new Student(2, "Pulkit", 56, "Male", "Computer Engineering", "Delhi", 67, Arrays.asList("+912632632762", "+1673434723929")),
                new Student(3, "Ankit", 25, "Female", "Mechanical Engineering", "Kerala", 164, Arrays.asList("+912632633882", "+1673434709929")),
                new Student(4, "Satish Ray", 30, "Male", "Mechanical Engineering", "Kerala", 26, Arrays.asList("+9126325832782", "+1671434729929")),
                new Student(5, "Roshan", 23, "Male", "Biotech Engineering", "Mumbai", 12, Arrays.asList("+012632632782","+56wd434729929")),
                new Student(6, "Chetan", 24, "Male", "Mechanical Engineering", "Karnataka", 90, Arrays.asList("+9126254632782", "+16736784729929")),
                new Student(7, "Arun", 26, "Male", "Electronics Engineering", "Karnataka", 324, Arrays.asList("+912632632782", "+1671234729929")),
                new Student(8, "Nam", 31, "Male", "Computer Engineering", "Karnataka", 433, Arrays.asList("+9126326355782", "+1673434729929")),
                new Student(9, "Sonu", 27, "Female", "Computer Engineering", "Karnataka", 7, Arrays.asList("+9126398932782", "+16563434729929", "+5673434729929")),
                new Student(10, "Shubham", 26, "Male", "Instrumentation Engineering", "Mumbai", 98, Arrays.asList("+912632646482", "+16734323229929")))
                .collect(Collectors.toList());

        //Find whose rank is above 100
        studentList.stream().filter(student -> student.getRank()>100).map(Student::getFirstName).forEach(System.out::print);

        // 1. Find the list of students whose rank is in between 50 and 100
       List<Student>  s= studentList.stream().filter(e->e.getRank() >=50 && e.getRank()<=100).collect(Collectors.toList());
        System.out.println(s);
        //2.Find the Students who stays in Karnataka and sort them by their names
       List<Student> s1= studentList.stream().filter(e->e.getCity()=="Karnataka")
                .sorted(Comparator.comparing(Student::getFirstName)).collect(Collectors.toList());
        System.out.println(s1);
        //3. Find all dept names
        List<String> s3= studentList.stream().map(Student::getDept).distinct().collect(Collectors.toList());
        System.out.println(s3);
        //4.  Find all the contact numbers
        List<List<String>> collect = studentList.stream().map(Student::getContacts).distinct().collect(Collectors.toList());
        System.out.println(collect);
        //Check if phno present or not
        String s4 = studentList.stream()
                .flatMap(e -> e.getContacts().stream().filter(e1 -> e1.equals("+1671434729929")))
                .findAny().get();
        System.out.println(s4);
        //5. Group The Student By Department Names
      var str= studentList.stream().collect(Collectors.groupingBy(Student::getDept));
        System.out.println(str);
        //6. Find the department who is having maximum number of students
       var str1= studentList.stream().collect(Collectors.groupingBy(Student::getDept, Collectors.counting())).entrySet()
                .stream().max(Map.Entry.comparingByValue()).get();
        System.out.println(str1);
        //7. avg age male and female emp
        studentList.stream()
                .collect(Collectors.groupingBy(Student::getGender, Collectors.averagingDouble(Student::getAge)))
                .entrySet()
                .forEach(e -> System.out.println(e.getKey() + " :" + e.getValue()));

        //8. Find the highest rank in each department
        studentList
                .stream().collect(Collectors.groupingBy(Student::getDept, Collectors.maxBy(Comparator.comparing(Student::getRank))))
                .entrySet().stream().forEach(e-> System.out.println(e.getKey()+" "+e.getValue()));
        //9.Find the student who has second highest rank
        var res = studentList.stream()
                .sorted(Comparator.comparing(Student::getRank).reversed())
                .skip(1)
                .findFirst();
        System.out.println(res);
    }
}
