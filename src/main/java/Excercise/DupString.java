package Excercise;

import java.util.Arrays;
import java.util.stream.Collectors;

public class DupString {
    public static void main(String[] args) {
        String s = "There is tree tree has leaves leaves are green";
        Arrays.stream(s.split(" ")).collect(Collectors.groupingBy(String::toString, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(v -> v.getValue() > 1)
                .forEach(v -> System.out.println(v.getKey() + ":" + v.getValue()));
    }
}
