package Excercise;

public class BuyAndSellStock {
    public void maxProfit(int[] prices) {
        int min = Integer.MAX_VALUE;
        int profit =0;
        for (int i : prices) {
            if (i < min)
                min = i;
            if (i - min > profit)
                profit = i - min;
        }
        System.out.println(profit);
    }
    public static void main(String[] args) {
        int[] prices = {7,1,5,3,6,4};
        new BuyAndSellStock().maxProfit(prices);

    }
}

//Input: prices = [7,1,5,3,6,4]
//        Output: 5
//        Explanation: Buy on day 2 (price = 1) and sell on day 5 (price = 6), profit = 6-1 = 5.
//        Note that buying on day 2 and selling on day 1 is not allowed because you must buy before you sell.