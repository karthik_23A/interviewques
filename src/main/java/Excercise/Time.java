package Excercise;

import java.util.Scanner;

public class Time {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the time when the watch was set correctly (HHMM): ");
        String set_time = scanner.next();
        System.out.print("Enter the number of seconds gained every 5 minutes: ");
        int gain_per_5_minutes = scanner.nextInt();
        System.out.print("Enter how many minutes X reached early: ");
        int minutes_early = scanner.nextInt();

        if (isValidInput(set_time, gain_per_5_minutes, minutes_early)) {
            String actual_time = calculateActualTime(set_time, gain_per_5_minutes, minutes_early);
            System.out.println("Actual time when X reached the interview venue: " + actual_time);
        } else {
            System.out.println("Invalid input");
        }

        // Close the scanner
        scanner.close();
    }

    public static boolean isValidInput(String set_time, int gain_per_5_minutes, int minutes_early) {
        if (set_time.length() != 4) {
            return false;
        }
        if (gain_per_5_minutes <= 0) {
            return false;
        }
        if (minutes_early < 0) {
            return false;
        }

        return true;
    }

    public static String calculateActualTime(String set_time, int gain_per_5_minutes, int minutes_early) {
        int set_time_minutes = Integer.parseInt(set_time.substring(0, 2)) * 60 + Integer.parseInt(set_time.substring(2));
        int total_gain = (minutes_early * 60) + (gain_per_5_minutes * (minutes_early / 5));
        int actual_time_minutes = set_time_minutes + total_gain;
        int actual_time_hours = actual_time_minutes / 60;
        int actual_time_minutes_part = actual_time_minutes % 60;
        return String.format("%02d%02d", actual_time_hours, actual_time_minutes_part);
    }
}