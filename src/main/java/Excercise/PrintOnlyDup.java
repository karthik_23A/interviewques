package Excercise;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class PrintOnlyDup {
    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 1, 5, 3, 9, 2, 1, 11, 19, 11, 1, 0, 1};
//        int num=6840;
//        num.
        //print number of 1 in array
        Arrays
                .stream(arr)
                .boxed()
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getKey() == 1)
                .forEach(e -> System.out.println(e.getValue()));

        Map<Integer, Long> frequencyMap = Arrays.stream(arr)
                .boxed()
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()));

//        frequencyMap.entrySet()
//                .stream()
//                .filter(e -> e.getValue() > 1)
//                .forEach(e-> System.out.println(e.getKey()+" "+e.getValue()));
//        System.out.println(findDuplicates(arr));

//        for (int i=0;i< arr.length;i++){
//            for (int j=i+1;j< arr.length;j++){
//                if (arr[i] == arr[j])
//                    System.out.println(arr[i]);
//            }
//        }

//        List<Integer> collect = Arrays.stream(arr)
//                .boxed() // Convert int to Integer
//                .collect(Collectors.groupingBy(Integer::intValue, Collectors.counting()))
//                .entrySet()
//                .stream()
//                .filter(entry -> entry.getValue() > 1)
//                .map(Map.Entry::getKey)
//                .collect(Collectors.toList());
//        System.out.println(collect);
    }

//    public static List<Integer> findDuplicates(int[] arr) {
//        Map<Integer, Long> frequencyMap = Arrays.stream(arr)
//                .boxed()
//                .collect(Collectors.groupingBy(e->e, Collectors.counting()));
//
//        frequencyMap.entrySet()
//                .stream()
//                .filter(entry -> entry.getValue() > 1)
//                .forEach(e-> System.out.println(e.getKey()));

//        return duplicates;
//    }

}

