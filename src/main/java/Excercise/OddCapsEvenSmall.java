package Excercise;

public class OddCapsEvenSmall {
    public static void main(String[] args) {
        String str = "conjecture presented to me";
        String[] splitt = str.split(" ");
        for (int k = 0; k < splitt.length; k++) {
            char[] i = splitt[k].toCharArray();
            for (int j = 0; j < i.length; j++) {
                if (j % 2 == 0)
                    System.out.print(String.valueOf(i[j]).toUpperCase());
                else
                    System.out.print(String.valueOf(i[j]).toLowerCase());
            }
            System.out.print(" ");
        }
    }
}
