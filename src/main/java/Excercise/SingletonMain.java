package Excercise;

public class SingletonMain {

    private static SingletonMain instance;

    private SingletonMain (){}

    public static SingletonMain getInstance(){
        if (instance == null){
            instance = new SingletonMain();
        }
        return instance;
    }

    public static void main(String[] args) {

    }
}
