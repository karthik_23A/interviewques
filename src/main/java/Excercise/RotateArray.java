package Excersice;

public class RotateArray {
    //arr = {1,2,3,4,5}
    //rotateCount = 2
    //o/p = {3,4,5,1,2}

    static void rotateArray(int arr[]) {
        int temp1=arr[0],n=arr.length-1;
        for (int i = 0; i < n; i++) {
            arr[i]=arr[i+1];
        }
        arr[n]=temp1;

    }

    public static void main(String[] args) {
        int arr[] = {1, 2, 3, 4, 5};
        int rotateCount = 2;
        for (int i = 0; i < rotateCount; i++) {
            rotateArray(arr);
        }
        

    }
}

