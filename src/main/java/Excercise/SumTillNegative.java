package Excercise;

import java.util.Scanner;

public class SumTillNegative {
    public static void main(String[] args) {
        int num;
        int sum = 0;
        do {
            num=new Scanner(System.in).nextInt();
            sum+=num;
        } while (num>=0);
        System.out.println(sum);
    }
}
