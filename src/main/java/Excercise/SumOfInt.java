package Excercise;

public class SumOfInt {
    public static void main(String[] args) {
        String input= "1 Rental $70000Shopping $299Expenses houses$200000";
        String[] sp = input.split("\\$");

        int sum=0;
        for (String str:sp){
            if (str.equals("\\D"))
                continue;
            try {
                int num = Integer.parseInt(str);
                sum+=num;
            }catch (Exception e){e.printStackTrace();}
        }
        System.out.println(sum);
    }
}
