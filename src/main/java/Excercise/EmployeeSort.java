package Excercise;

import java.util.Comparator;

public class EmployeeSort {
    public static void main(String[] args) {
//        Optional<Employee> sec
        EmployeeDb.getEmployees()
                .stream()
                .sorted(Comparator.comparing(Employee::getName).reversed())
                .findFirst()
                .ifPresent(System.out::println);
    }
}
