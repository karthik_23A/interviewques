package Excercise;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class UniqueEleInArr {

    static void remDupFrmArr(int[] arr){

        List<Integer> uniqueList = new ArrayList<>();
        for (int num : arr) {
            if (!uniqueList.contains(num)) {
                uniqueList.add(num);
            }
        }
//        int[] uniqueArr = new int[uniqueList.size()];
//        for (int i = 0; i < uniqueList.size(); i++) {
//            uniqueArr[i] = uniqueList.get(i);
//        }

        // Print the unique elements
       // System.out.println(uniqueList);
    }
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 2, 6, 3, 9, 12, 7, 1};
        remDupFrmArr(arr);

        // Prem Test
        List<Integer> a= Arrays.stream(arr).boxed().collect(Collectors.toSet()).stream().collect(Collectors.toList());
        List<Integer> collect2 = Arrays.stream(arr).boxed().distinct().collect(Collectors.toList());
        System.out.println(collect2);
        Arrays.stream(arr).boxed().collect(Collectors.groupingBy(e->e,Collectors.counting()))
                .entrySet().stream().filter(e->e.getValue()>1).forEach(e-> System.out.print(e.getKey()));
        //        int i, j;
//        boolean isUnique;
//
//        for (i = 0; i < arr.length; i++) {
//            isUnique = true;
//            // Compare the current element with every other element
//            for (j = 0; j < arr.length; j++) {
//                if (i != j && arr[i] == arr[j]) {
//                    isUnique = false;
//                    break;
//                }
//            }
//            // If the element is unique, print it
//            if (isUnique) {
//                System.out.println(arr[i]);
//            }
//        }
    }
}
