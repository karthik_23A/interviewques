package Excercise;

public class SinglyLinkedList<T> {
    private Node<T> head;
    private Node<T> tail;

    private static class Node<T> {
        T data;
        Node<T> next;


        /**
         * Creates a new node with the given data.
         *
         * @param data the data to be stored in the node
         */
        Node(T data) {
            this.data = data;
            this.next = null;
        }
    }

    /**
     * Adds an element to the end of the linked list.
     *
     * @param data the data to be added to the list
     */
    public void add(T data) {
        Node<T> newNode = new Node<>(data);
        if (head == null) {
            head = newNode;
        } else {
            Node<T> current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
    }

    public void addAtLast(T data) {
        Node<T> newNode = new Node<>(data);
        if (head == null) {
            head = newNode;
        } else {
            Node<T> current = head;
            while (current.next != null) {
                current = current.next;
            }
            current.next = newNode;
        }
    }


    public void addLast(T data){
        if (tail== null){
            add(data);
            return;
        }
        else {
            Node<T> node = new Node<>(data);
            tail.next= node;
            tail=node;
        }
    }

    /**
     * Displays the elements of the linked list in order.
     */
    public void display() {
        Node<T> current = head; //current is like a temp var
        while (current != null) {
            System.out.print(current.data + " -> ");
            current = current.next;
        }
        System.out.println("null");
    }

    public boolean isEmpty() {
        return head == null;
    }

    /**
     * Deletes the first occurrence of a specified element from the list.
     *
     * @param data the data to be deleted from the list
     */
    public void delete(T data) {
        if (head == null) {
            return;
        }
        if (head.data.equals(data)) {
            head = head.next;
            return;
        }

        Node<T> current = head;
        while (current.next != null && !current.next.data.equals(data)) {
            current = current.next;
        }

        if (current.next != null) {
            current.next = current.next.next;
        }
    }

    public static void main(String[] args) {
        SinglyLinkedList<Integer> list = new SinglyLinkedList<>();

        list.add(1);
        list.add(2);
        list.add(3);
        list.addAtLast(9);
        list.add(4);

        System.out.println("Linked List: ");
        list.display();

        list.delete(2);
        System.out.println("After deleting 2: ");
        list.display();
    }
}

