package Excercise;

public class MultiThreadEx implements Runnable {

    @Override
    public void run() {
        System.out.println("planning " + Thread.currentThread().getName());
        System.out.println("Designing " + Thread.currentThread().getName());
        System.out.println("Develop " + Thread.currentThread().getName());
        System.out.println("Testing " + Thread.currentThread().getName());
        System.out.println("Deploy " + Thread.currentThread().getName());
    }
    public static void main(String[] args) throws InterruptedException {
        MultiThreadEx m = new MultiThreadEx();
        for (int i = 0; i < 2; i++) {
            Thread t = new Thread(m);
            t.start();
            t.join();
            System.out.println("hi");


        }
    }
}
