package Excercise;

public class AvgForStu2DArray {
    public static void printAvg(String[][] data){
        int len = data.length-1;
        int count =1,sum=0;
        for (int i =0;i<len;i++){
            for (int j =0;j<len;j++){
                if (data[i][j].equals(data[i + 1][j])){
                    count++;
                    sum = Integer.parseInt(data[i][1]) + Integer.parseInt(data[i+1][1]);
                }
                int avg = sum/count;
                System.out.println(avg);
            }
        }
    }

    public static void main(String[] args) {
        String[][] data = {{"a","2"},{"b","20"},{"c","8"},{"a","3"},{"a","2"},{"c","6"}};
        printAvg(data);

    }
}
