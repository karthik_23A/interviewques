package Excercise;

import java.util.*;
import java.util.stream.Collectors;

public class NoOfOccurenceOfWordInStr {
    public static void main(String[] args) {
        // USING HASHMAP TO IMPLEMENT
        System.out.println("Using Hashmap implementation:");
        String str1 = "Work while Work and play while play";
        HashMap<String,Integer> vars=new HashMap<>();
        String[] strAr1 = str1.split(" ");

        for (String word: strAr1){
//            Integer in =vars.get(word);

            if (vars.containsKey(word)){
                vars.put(word,vars.get(word)+1);
            }
            else {
                vars.put(word,1);
            }
        }
//        System.out.println(vars);

//        using Java8 to implement
        String str = "Work while Work and play while Play";
        Arrays.stream(str.split(" "))
                .collect(Collectors.groupingBy(e->e,Collectors.counting()))
                .entrySet()
                .stream()
//                .filter(e->e.getValue()>1)
                .forEach(System.out::println);
//        String[] words = str.split(" ");
//        List<String> words1 = Arrays.asList(words);
//
//        Map<String, Long> res = words1.stream().collect(Collectors.groupingBy(e -> e, Collectors.counting()));
//        System.out.println(res);
    }
}
