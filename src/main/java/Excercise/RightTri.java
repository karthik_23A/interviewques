package Excercise;

public class RightTri {
    public void printTri(int n){
        for (int rows = 0; rows < n; rows++){
            for (int col = 0; col <= rows; col++){
                System.out.print("* ");
            }
            System.out.println();
        }
    }
    public static void main(String[] args) {
        int n = 5;
        new RightTri().printTri(n);
    }
}
