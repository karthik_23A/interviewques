package Excercise;

public class Fruit {
    public void name() {
        System.out.println("Apple");
    }
}
class Orange extends Fruit{

    //overriding
    public void name(){
        super.name();
        System.out.println("Orange");;
    }

    //overloading
    public int add(int a,int b){
        return a+b;
    }
    public int add(int a,int b,int c){
        return a+b+c;
    }

    public static void main(String[] args) {
        new Orange().name();
        System.out.println(new Orange().add(1,2));
        System.out.println(new Orange().add(1,2,1));
    }
}
