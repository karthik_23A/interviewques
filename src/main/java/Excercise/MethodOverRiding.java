package Excercise;

public class MethodOverRiding {
     static void displayName(){
        System.out.println("Karthik");
    }
}
class DerivedClass extends MethodOverRiding{
     static void displayName() {
//        super.displayName();
        System.out.println("Arjun");
    }
    public static void main(String[] args) {
//        MethodOverRiding d = new MethodOverRiding();
        MethodOverRiding e = new DerivedClass();
//        d.displayName();
        e.displayName();

    }
}
