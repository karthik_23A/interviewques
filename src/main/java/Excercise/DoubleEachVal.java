package Excercise;

public class DoubleEachVal {
    public static void main(String[] args) {
        //double each val
        int a=12345;
        String.valueOf(a)
                .chars()
                .map(Character::getNumericValue)
                .map(num->num*num)
                .forEach(System.out::println);
    }
}
