package Excercise;

import java.util.Comparator;
import java.util.List;

public class JavaStreamsEg {
//    private static Employee getEmpByName(String name) throws Exception {
//        List<Employee> employees = EmployeeDb.getEmployees();
        // Optional class
        // find employee data by id also id allowed to be null by using Optionl.nullable()
//        return employees.
//                stream().
//                filter(emp->emp.getName().equalsIgnoreCase(String.valueOf(name)))
//                .findAny()
//                .orElseThrow(()-> new Exception("no Employee found with this name"));
//    }
    public static void main(String[] args) throws Exception {
        List<Employee> da = EmployeeDb.getEmployees();
//        da.sort(Comparator.comparingInt(Employee::getSalary));
//        System.out.print(da);
//
//        //Stream method used to do custom sort with salary
        da.stream()
                .sorted(Comparator.comparingInt(Employee::getSalary))
                .skip(da.size()-1)
//                .map(Employee::getName)
                .forEach(System.out::println);



//        Employee emp = new Employee(1,"abs",29600);
//        getEmpByName("Karthik");
//        try{
//            Optional<String> optionalName = Optional.of(emp.getName());
//            System.out.println(optionalName);
//            Optional<String> optNull = Optional.ofNullable(emp.getName());
//            System.out.println(optNull);
//        }catch (Exception e){
//            return;
//        }





    }
}
