package Excercise;

public class Clone implements Cloneable{
    String name;
    int age;

    public Clone(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public static void main(String[] args) throws CloneNotSupportedException {
        Clone cl = new Clone("Karthik",24);
        Clone cl2 = (Clone) cl.clone();
        System.out.println(cl.name+" "+cl.age);
        System.out.println(cl2.name+" "+cl2.age);
    }
}
