package Excercise;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;

public class HighSalInEachDept {
    private int id;
    private String name;
    private String dept;
    private Long salary;

    public HighSalInEachDept(int id, String name, String dept,Long salary) {
        this.id = id;
        this.name = name;
        this.dept = dept;
        this.salary=salary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "HighSalInEachDept{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dept='" + dept + '\'' +
                ", salary='" + salary + '\'' +
                '}';
    }

    public static void main(String[] args) {
        List<HighSalInEachDept> li = new ArrayList<>();
        li.add(new HighSalInEachDept(1,"Karthik","IT",10L));
        li.add(new HighSalInEachDept(2,"Guru","CSE",30L));
        li.add(new HighSalInEachDept(3,"Prem","IT",12L));
        li.add(new HighSalInEachDept(4,"Siva","IT",9L));
        li.add(new HighSalInEachDept(5,"Santhosh","CSE",18L));

//        Map<String, Object> collect = li.stream().collect(groupingBy(HighSalInEachDept::getDept,
//                Collectors.collectingAndThen(Collectors.maxBy(Comparator.comparingLong(HighSalInEachDept::getSalary)), Optional::get)));
        li.stream().collect(Collectors.groupingBy(HighSalInEachDept::getDept,Collectors.maxBy(Comparator.comparing(HighSalInEachDept::getSalary))))
                .entrySet().forEach(e-> System.out.println(e.getKey()+": "+e.getValue().get()));

//        System.out.println(collect);
    }
}
