package Excercise;

import java.util.List;
import java.util.stream.Collectors;

public class UniqueChars {
    public static void main(String[] args) {
        String str = "Hello world";
        List<Integer> num=List.of(10,15,5,10,98,5,98,32,180,18);
        //mapToObj() method to convert each character code back to a Character object.


        str.chars()
                .mapToObj(c->(char)c)
                .filter(whiteSpace->!Character.isWhitespace(whiteSpace))
                .collect(Collectors.groupingBy(e->e,Collectors.counting()))
                .forEach((k,v)-> System.out.println(k+":"+v));

        // print duplicate numbers
//       num.stream()
//               .collect(Collectors.groupingBy(e->e,Collectors.counting()))
//               .entrySet()
//               .stream()
//               .filter(e->e.getValue()>1)
//               .forEach(s-> System.out.println(s.getKey()));
    }

}
