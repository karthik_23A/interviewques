package Excercise;

import java.util.*;

public class Print2ndLargest {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 5, 2, 0,9,3};
//        int[] arr1 = {};
//        secLar(arr);
        secLargest(arr);
    }

    static void secLargest(int arr[]) {
        List<Integer> ele = new ArrayList<>();
        for (int i = 0; i < arr.length; i++) {
            ele.add(arr[i]);
        }
        Set<Integer> dup = new TreeSet<>(ele);
        ele.clear();
        for (int j : dup) {
            ele.add(j);
        }
        int size = ele.size();
        System.out.println(ele.get(size-1));

//        int i = Arrays.stream(arr).max().orElse(Integer.MIN_VALUE);
//        System.out.println(i);
    }
}
