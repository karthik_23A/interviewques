package Excercise;

// Java implementation of iterative Binary Search
class FindValInGivenArr {
    // Returns index of x if it is present in arr[l....r], else return -1
    int binarySearch(int arr[], int left, int right, int val)
    {
        //{ 2, 3, 4, 10, 40 };
        while (left <= right) {
            int mid = (left + right) / 2;

            // If the element is present at the
            // middle itself
            if (arr[mid] == val) {
                return mid;

                // If element is smaller than mid, then
                // it can only be present in left subarray
                // so we decrease our r pointer to mid - 1
            } else if (arr[mid] > val) {
                right = mid - 1;

                // Else the element can only be present
                // in right subarray
                // so we increase our l pointer to mid + 1
            } else {
                left = mid + 1;
            }
        }

        // We reach here when element is not present
        // in array
        return -1;
    }

    // Driver method to test above
    public static void main(String args[])
    {
        FindValInGivenArr ob = new FindValInGivenArr();

        int arr[] = { 2, 3, 4, 10, 40 };
        int n = arr.length;
        int x = 10;
        int result = ob.binarySearch(arr, 0, n - 1, x);

        if (result == -1)
            System.out.println("Element not present");
        else
            System.out.println("Element found at index "
                    + result);
    }
}
