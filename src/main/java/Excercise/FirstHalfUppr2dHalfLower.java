package Excercise;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class FirstHalfUppr2dHalfLower {
    public static void main(String[] args) {
//        String str = "This is java program THIS IS JAVA PROGRAM";
//
//        int mid = str.length() / 2;
//
//        String toUpper = str.substring(0, mid).toUpperCase();
//        System.out.print(toUpper);
//
//        String toLower = str.substring(mid + 1).toLowerCase();
//        System.out.println(toLower);

        List<String> ele = new ArrayList<>();
        ele.add("a");
        ele.add("b");
        ele.add("c");
        ele.add("d");
        ele.add("e");
        List<String> collect = ele.stream().limit(3).map(String::toUpperCase).collect(Collectors.toList());
        //List<String> collect =   ele.stream().skip(3).map(String::toUpperCase).collect(Collectors.toList());
        ele.subList(0,3).clear();
        ele.addAll(0,collect);
        ele.forEach(System.out::print);
    }
}
