package Excercise;

import java.util.Arrays;
import java.util.stream.Collectors;

public class ComnEleIn2Arr {
    public static void main(String[] args) {
        int[] a1 = {2, 5, 3, 8, 1};
        int a[] = {98, 8910, 7};
        int[] a2 = {3, 2, 12, 14};
        String s = "kaviraj is a gay";
//        for (int i = 0; i < a1.length-1; i++) {
//            for (int j = 0; j < a2.length-1; j++) {
//                if (a1[i]==a2[j])
//                    System.out.print(a1[i]+" ");
//            }
        // 98,7,6 9876
        // String.valueOf(s).chars().collect(ch -> ch,Collectors.counting())

        s.chars().mapToObj(e -> (char) e).collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                .entrySet().stream().filter(e -> e.getValue() == 1).map(entry -> entry.getKey().toString())
                .collect(Collectors.joining());
        //.forEach(e-> System.out.print(e.getKey()));
//          Arrays.stream(a).boxed().sorted(Comparator.reverseOrder()).forEach(System.out::print);

//          int[] a1 = {2, 5, 3, 8, 1};
//        int[] a2 = {3, 2, 12, 14};
       var res = Arrays.stream(a1).filter(z -> Arrays.stream(a2).anyMatch(b -> z == b));
        System.out.println(res.boxed().findFirst().get());

        //}


    }
}
