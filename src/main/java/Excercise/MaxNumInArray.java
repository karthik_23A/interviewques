package Excercise;

import java.util.Arrays;

public class MaxNumInArray {
    public static void maxNum(int[] arr){
        int len = arr.length;
        int max =0;
        if (len < 2){
            return;
        }
        for (int i = 0; i < len-1; i++){
                if (arr[i]<arr[i+1]){ //3<4
                    max = arr[i+1];//max=4
                    arr[i] = max;//arrr[i]=4
                }else {
                    max = arr[i];
                    arr[i+1] = max;
                }
        }
        System.out.println(max);
//        int maxi = Arrays.stream(arr).max().orElse(-1);
//        System.out.println(maxi);
    }
    public static void main(String[] args) {
        int[] arr = {5,1,13,40,2,6};

        int a[]= Arrays.stream(arr)
                .map(num -> {
                    if (num >= 10) return Arrays.stream(Integer.toString(num).split(""))
                                .mapToInt(Integer::parseInt)
                                .sum();

                    else return num;

                })
                .toArray();
        //        Arrays.stream(arr).max().stream().forEach(System.out::println);
        //maxNum(arr);
    }
}
