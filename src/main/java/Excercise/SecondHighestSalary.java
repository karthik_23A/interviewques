package Excercise;

import java.util.*;

public class SecondHighestSalary {
    public static void main(String[] args) {
        Map<String, Integer> salaryMap = new HashMap<>();

        // Populate the HashMap with employee salaries
        salaryMap.put("Alice", 50000);
        salaryMap.put("Bob", 60000);
        salaryMap.put("Guru", 75000);
        salaryMap.put("Charlie", 55000);
        salaryMap.put("David", 80000);
        salaryMap.put("Eve", 65000);
        salaryMap.put("Karthik", 65000);

        List<Integer>  num = new ArrayList<>(Arrays.asList(5,7,4,9,12,3,1));
        Optional<Integer> collect = num.stream().sorted(Comparator.reverseOrder()).skip(1).findFirst();
        System.out.println(collect);

        Optional<Map.Entry<String, Integer>> entryStream = salaryMap.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).skip(1).findFirst();;

//        Optional<Map.Entry<String, Integer>> sortedEntries = entryStream
//                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder())).skip(1).findFirst();
//                .collect(Collectors.toList());
        System.out.println(entryStream);

//        int secondHighestSalary = sortedEntries.get(1).getValue();
//
//        List<String> secondHighestEmployees = sortedEntries
//                .stream()
//                .filter(entry -> entry.getValue() == secondHighestSalary)
//                .map(entry -> "Key: " + entry.getKey() + ", Value: " + entry.getValue())
//                .collect(Collectors.toList());
//        System.out.println(secondHighestEmployees);

    }
}
