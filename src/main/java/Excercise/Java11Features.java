package Excercise;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

public class Java11Features {
    public static void main(String[] args) throws IOException {
        // easy way to read/write strings from files
        Path path= Files.writeString(Files.createTempFile(Path.of("D:\\samFolder"),"test",".txt"),"Hi");
//        System.out.println(path);
        String s=Files.readString(path);
//        System.out.println(s);

        //collection to array
        List<String> sampleList = Arrays.asList("Java", "Kotlin");
        String[] sampleArray = sampleList.toArray(String[]::new);
//        System.out.println(Arrays.toString(sampleArray));

        String[] arr={"a","b","c"};
//        Arrays.stream(arr).map(String::toUpperCase).forEach(System.out::println);

        List<Integer> ele = List.of(8,12,18,45,36,19,91);
        ele
                .stream()
                .filter(num->String.valueOf(num).endsWith("1"))
                .forEach(System.out::println);

    }
}
