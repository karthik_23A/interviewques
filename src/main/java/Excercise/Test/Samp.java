package Excercise.Test;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Samp {
    public static void main(String[] args) {
        String str1 = "Work while work and play while play";
        Arrays.stream(str1.split(" "))
                .collect(Collectors.groupingBy(e -> e, Collectors.counting()))
                .entrySet()
                .stream()
                .filter(e -> e.getValue() > 1)
                .forEach(s-> System.out.println(s.getKey()+" "+s.getValue()));
//        System.out.println(res);
    }
}
