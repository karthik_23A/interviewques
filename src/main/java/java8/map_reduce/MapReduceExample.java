package java8.map_reduce;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

public class MapReduceExample {

    public static void main(String[] args) {

        List<Integer> numbers = Arrays.asList(3, 7, 8, 1, 5, 9);

        List<String> words = Arrays.asList("corejava", "spring", "hibernate");

        int sum = 0;
        for (int no : numbers) {
            sum = sum + no;
        }
//        System.out.println(sum);

        int sum1 = numbers.stream().mapToInt(i -> i).sum();
//        System.out.println(sum1);

        Integer reduceSum = numbers.stream().reduce(0, (a, b) -> a + b);
//        System.out.println(reduceSum);

//        Optional<Integer> reduceSumWithMethodReference = numbers.stream().reduce(Integer::sum);
//        System.out.println(reduceSumWithMethodReference.get());
//
        Integer mulResult = numbers.stream().reduce(1, (a, b) -> a * b);
//        System.out.println(mulResult);
//
//        Integer maxvalue = numbers.stream().reduce(0, (a, b) -> a > b ? a : b);
//        System.out.println(maxvalue);
//
        Integer maxvalueWithMethodReference = numbers.stream().reduce(Integer::max).get();
//        System.out.println(maxvalueWithMethodReference);
//
//
//        String longestString = words.stream()
//                .reduce((word1, word2) -> word1.length() > word2.length() ? word1 : word2)
//                .get();
//        System.out.println(longestString);
//
//        //get employee whose grade A
//        //get salary
        double avgSalary = EmployeeDatabase.getEmployees().stream()
                .filter(employee -> employee.getGrade().equalsIgnoreCase("A"))
                .map(Employee::getSalary)
                .mapToDouble(i -> i)
                .average().getAsDouble();
//        System.out.println(avgSalary);
//
        double sumSalary = EmployeeDatabase.getEmployees().stream()
                .filter(employee -> employee.getGrade().equalsIgnoreCase("A"))
                .map(Employee::getSalary)
                .mapToDouble(i -> i)
                .sum();
//        System.out.println(sumSalary);


        //get avg salary based on gender
        Map<String, Double> collect = EmployeeDatabase.getEmployees().stream()
                .collect(groupingBy(Employee::getSex, averagingDouble(Employee::getSalary)));
//        System.out.println(collect);

        //based on id need to sort
//        EmployeeDatabase.getEmployees()
//                .stream()
//                .sorted(Comparator.comparingInt(Employee::getId)).forEach(s->System.out.println("Id: "+s.getId()));

        //based on city need to return emp name
        Map<String, List<String>> collect1 = EmployeeDatabase.getEmployees().stream()
                .collect(groupingBy(Employee::getCity, mapping(Employee::getName,toList())));
//        System.out.println(collect1);

          EmployeeDatabase.getEmployees()
                .stream()
                .collect(groupingBy(Employee::getGrade, averagingDouble(Employee::getSalary)));
//        System.out.println(collect2);

    }


}
