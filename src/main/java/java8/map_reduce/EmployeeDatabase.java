package java8.map_reduce;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class EmployeeDatabase {


    public static List<Employee> getEmployees(){
      return  Stream.of(new Employee(101,"Karthik","Male","A",60000,"Chennai"),
              new Employee(109,"Harish","Female","B",30000,"Banglore"),
              new Employee(102,"Guru","Female","A",80000,"Chennai"),
              new Employee(103,"Teja","Male","A",90000,"Hyd"),
              new Employee(104,"Mahesh","Male","C",15000,"Hyd"),
              new Employee(106,"Santhosh","Male","C",15000,"Chennai"))
              .collect(Collectors.toList());
    }
}
