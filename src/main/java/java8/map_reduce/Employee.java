package java8.map_reduce;

public class Employee {
    private int id;
    private String name;
    private String sex;
    private String grade;
    private double salary;
    private String city;

    public Employee() {
    }

    public Employee(int id, String name, String sex, String grade, double salary, String city) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.grade = grade;
        this.salary = salary;
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String salary) {
        this.city = city;
    }


}
