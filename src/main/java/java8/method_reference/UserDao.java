package java8.method_reference;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserDao {
    public static List<User> getUserData(){
//        List<User> dada = new ArrayList<>();
//        dada.add(new User("Arjun",6));
//        dada.add(new User("Karthik",24));
//        dada.add(new User("Saravana",21));
//        dada.add(new User("Guru",24));
//        dada.add(new User("Saranya",23));

        return Stream.of(new User("Arjun",6),
                new User("Karthik",24),
                new User("Saravana",21),
                new User("Guru",24),
                new User(" ",2),
                new User("Saranya",32)).collect(Collectors.toList());
    }

//    public static List<User> getInvalidFormatName(){
//        return getUserData().stream().filter(u-> !u.getName().equals(" ")).collect(Collectors.toList());
//    }

}
