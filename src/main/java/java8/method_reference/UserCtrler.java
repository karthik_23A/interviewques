package java8.method_reference;

import java.util.*;

public class UserCtrler {
    public static void main(String[] args) {

//        boolean isReal = UserDao.getUserData().stream().anyMatch(UserDao::getInvalidFormatName);

            List<User> datas = UserDao.getUserData();
            datas.stream().filter(user -> !user.getName().startsWith(" ")).forEach(System.out::println);
//            datas.forEach(System.out::println);
    }
}
