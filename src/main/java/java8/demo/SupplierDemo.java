package java8.demo;

import java.util.Arrays;
import java.util.List;
import java.util.function.Supplier;

public class SupplierDemo {
	private static String getName(){
		return "hi";
	}

	public static void main(String[] args) {

		List<String> list1 = Arrays.asList("abs","xyz","hjk");

		System.out.println(list1.stream().findAny().orElseGet(SupplierDemo::getName));

		System.out.println(list1.stream().findFirst());
	}
}
