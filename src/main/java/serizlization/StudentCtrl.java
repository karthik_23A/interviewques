package serizlization;

import java.io.*;

public class StudentCtrl {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Student student = new Student("Karthik",24);
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("C:\\Users\\karth\\OneDrive\\Documents\\er.txt");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(student);
            objectOutputStream.flush();
            objectOutputStream.close();
            System.out.println("Done");
        }catch (Exception e){
            e.printStackTrace();
        }

        ObjectInputStream in=new ObjectInputStream(new FileInputStream("C:\\Users\\karth\\OneDrive\\Documents\\er.txt"));
        Student s=(Student)in.readObject();
        //printing the data of the serialized object
        System.out.println(s.getName()+" "+s.getAge());
        in.close();
    }
}
