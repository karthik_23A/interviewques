package json_parse;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import netscape.javascript.JSObject;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ReadFromJson {
    public static void main(String[] args) throws IOException, ParseException {
        FileReader f = new FileReader("src/main/java/json_parse/data.json");
        BufferedReader bf = new BufferedReader(f);
        JSONParser parser = new JSONParser();
        StringBuilder jsonString = new StringBuilder();
        String line;
        while ((line = bf.readLine()) != null) {
            jsonString.append(line);
        }
        bf.close();
        Object obj = parser.parse(jsonString.toString());

        if (obj instanceof JSONObject) {
            // Access data from a single object
            JSONObject jsonObject = (JSONObject) obj;
            // Get specific values using key
            String lasrNname = (String) jsonObject.get("lastName");
            Long age = (Long) jsonObject.get("age");

//        } else if (obj instanceof JSONArray) {
//            JSONArray jsonArray = (JSONArray) obj;
//            for (int i = 0; i < jsonArray.size(); i++) {
//                JSONObject jsonObject = (JSONObject) jsonArray.get(i);
//                var address = jsonObject.get("address");
//                if (address!= null){
//                    System.out.println(jsonObject.get("city"));
//                }
//            }
//        }
        }
    }
}
