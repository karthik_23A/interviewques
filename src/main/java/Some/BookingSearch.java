package Some;

import java.util.List;

public class BookingSearch {
    private String caseId;
    private String bookingId;
    private String bookingDate;

    public BookingSearch(){}

    public BookingSearch(String caseId, String bookingId, String bookingDate) {
        this.caseId = caseId;
        this.bookingId = bookingId;
        this.bookingDate = bookingDate;
    }

    public String getCaseId() {
        return caseId;
    }

    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public String getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(String bookingDate) {
        this.bookingDate = bookingDate;
    }

    @Override
    public String toString() {
        return "BookingSearch{" +
                "caseId='" + caseId + '\'' +
                ", bookingId='" + bookingId + '\'' +
                ", bookingDate='" + bookingDate + '\'' +
                '}';
    }
}
