package Some;

import java.util.ArrayList;
import java.util.List;

public class BookMain {

    public static void main(String[] args) {
        List<BookingSearch> list = new ArrayList<>();
        list.add(new BookingSearch("12", "1234", "03/12/2022"));

        StringBuilder jsonBuilder = new StringBuilder();
        jsonBuilder.append("{\"bookingSearch\":[{");

        for (int i = 0; i < list.size(); i++) {
            BookingSearch bookingSearch = list.get(i);
            jsonBuilder.append("\"caseId\":\"").append(bookingSearch.getCaseId()).append("\", ");
            jsonBuilder.append("\"bookingId\":\"").append(bookingSearch.getBookingId()).append("\", ");
            jsonBuilder.append("\"bookingDate\":\"").append(bookingSearch.getBookingDate()).append("\"");

            if (i < list.size() - 1) {
                jsonBuilder.append("}, {");
            }
        }

        jsonBuilder.append("}]}");
        String json = jsonBuilder.toString();

        System.out.println(json);

    }


}
